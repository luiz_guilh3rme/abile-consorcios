<?php 
	// Template Name: Template - Obrigado

include 'header.php';
?>
<section id="obrigado">
	<div class="content-ty">
		<div class="container">
			<div class="row">
				<div class="ty-page">
					<?php 
					if ( have_posts() ) : 
						while ( have_posts() ) : 
							the_post();
							?>
							<div class="title-tks">
								<?php the_content(); ?>		
							</div>
							<?php
						endwhile; 
					endif;
					?>
					<div class="pricing-block">
						<?php 

						$id = $_GET['id'];
						$terms = wp_get_post_terms($id, 'tipos-de-credito');
						$credit = get_post($id)->post_title; 
						// var_dump($terms);
						// var_dump(get_post($id));

						if ( isset($id) ) {	
							$priceDisplay = new WP_Query([
								'posts_per_page'	=> -1,
								'post_type'			=> 'valores',
								'post__in'			=> [$id],
							]);

							if ( $priceDisplay->have_posts() ) {
									
								while ( $priceDisplay->have_posts() ) {

									$arr = [];
									for ($i=120; $i <= 176; $i++) { 
									
										array_push($arr,$i);
									}
									// print_r($arr);

									if ( !empty(array_search($id, $arr)) ) {

										$priceDisplay->the_post();
										echo '<p class="item-desc-col margin">'; 
										echo '<strong>Parcela Convencional: </strong>';
										the_field('parcela_pj');
										echo '<br>'; 
										echo '<strong>Parcela com redutor de 30%: </strong>';
										the_field('parcela_pf');
										echo '<br>'; 
										echo '<strong>Crédito Selecionado: </strong>';
										echo str_replace('Crédito de','', $credit);
										echo '</p>';
									} else {

										$priceDisplay->the_post();
										echo '<p class="item-desc-col margin">'; 
										echo '<strong>Parcela PJ: </strong>';
										the_field('parcela_pj');
										echo '<br>'; 
										echo '<strong>Parcela PF: </strong>';
										the_field('parcela_pf');
										echo '<br>'; 
										echo '<strong>Crédito Selecionado: </strong>';
										echo str_replace('Crédito de','', $credit);
										echo '</p>';
									}

									// if(is_page($arr)){


									// }else{

									// 	$priceDisplay->the_post();
									// 	echo '<p class="item-desc-col margin">'; 
									// 	echo '<strong>Parcela PJ: </strong>';
									// 	the_field('parcela_pj');
									// 	echo '<br>'; 
									// 	echo '<strong>Parcela PF: </strong>';
									// 	the_field('parcela_pf');
									// 	echo '</p>';
									// }
								}
							}
							wp_reset_postdata();
						}
						?>
						<p class="btn-back"><a href="<?php echo home_url(); ?>">Clique aqui</a> para voltar a página principal.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include 'footer.php'; ?>