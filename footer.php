<footer>
	<div class="bg-footer">
		<div class="container">
			<div class="row">
				<div class="txt-footer">
					<p class="text-desk"><strong>Consórcio Porto Seguro</strong> - Todos os Direitos Reservados | Copyright 2019</p>
					<p class="text-mobile"><strong>Consórcio Porto Seguro</strong> Todos os Direitos Reservados Copyright 2019.</p>
				</div>
			</div>
		</div>	
	</div>
	<?php
get_template_part('includes/components/mobile-cta-wrapper');
get_template_part('includes/components/cta-wrapper');
get_template_part('includes/components/cta-parcela');
?>
</footer>
<?php 
get_template_part('includes/components/oldie'); 
wp_footer();
?>
</body>
</html> 
