<?php get_header(); ?>
<main class="structure">
	<div class="background-content"></div>
	<section id="content">
		<div class="container">
			<div class="row">
				<div class="col-2 boxes">
					<div class="main-left">
						<h1>Sua <strong>Casa</strong> ou <strong>Carro Novo</strong> com parcelas que cabem no<p class="dark-blue">SEU BOLSO!</p></h1>
						<img src="<?php echo $dir. '/dist/img/bar-red.png' ?>" alt="bar-red">

						<p class="text-firstleft">O Consórcio <strong>Porto Seguro</strong> é uma ótima opção para você comprar seu imóvel ou automóvel por meio de um consórcio. É uma maneira inteligente de aplicar o seu dinheiro e garantir a aquisição do seu bem.</p>
					</div>		
				</div>
				<div class="col-2 boxes">
					<div class="main-right">
						<div class="box-proposta">
							<div class="items-boxes">
								<p class="title-box">Solicite uma <p class="blue-titlebox">proposta</p></p>
								<p class="desc-box">Solicite já uma proposta de Consórcio Imobiliário e/ou Automotivo e comece a planejar os seus sonhos!</p>
								<button class="green-button btn-mb">Solicitar Proposta</button>
								<form class="form-box" data-modal="form-proposta">
									<input type="hidden" name="formulario" value="Proposta">
									<input type="text" name="nome" placeholder="Nome" required>
									<input type="mail" name="email" placeholder="E-mail" required>
									<input type="phone" name="celular" placeholder="DDD + Celular" required>
									<input type="tel" name="telefone" required minlength=14 placeholder="DDD + Telefone (opcional)">

									<select name="modelo" id="modelo">
										<option value="">Modelo Desejado</option>
										<option value="Imóvel">Consórcio de Imóvel</option>
										<option value="Automóvel">Consórcio de Automóvel</option>
										<option value="Caminhão">Consórcio de Caminhão</option>
										<option value="Ônibus">Consórcio de Ônibus</option>
										<option value="Máquinas Agrícolas">Consórcio de Máquinas Agrícolas</option>
									</select>

									<input type="text" name="credito" placeholder="Crédito Desejado">
									<button type="submit" name="proposta" class="green-button">Solicitar Proposta</button>
								</form>	
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="container">
				<div class="col-2">
					<p class="text-porto">Só na <strong>Porto Seguro</strong> você tem planos de parcelas com <span class="redutor">REDUTOR DE 30%</span> até a contemplação!</p>
				</div>		
				<div class="mini-items">
					<div class="slider">
						<div class="box-mini-item">
							<img class="img-items" src="<?php echo $dir. '/dist/img/items/credits.png'; ?>" alt="Créditos">
							<p class="item-desc-col txtslide-proposta">Válido para créditos de <strong>250 mil</strong> a <strong>2 milhões</strong></p>
						</div>
						<div class="box-mini-item align">
							<img class="img-items" src="<?php echo $dir. '/dist/img/items/contemplation.png'; ?>" alt="Créditos">
							<p class="item-desc-col txtslide-proposta">Após a contemplação resgate de <strong>70%</strong> a <strong>100% de todo o crédito</strong></p>
						</div>
						<div class="box-mini-item">
							<img class="img-items" src="<?php echo $dir. '/dist/img/items/fgts.png'; ?>" alt="Créditos">
							<p class="item-desc-col txtslide-proposta">Possibilidade de utilizar o <strong>saldo do FGTS</strong></p>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>
	<section id="imoveis">
		<div class="background-imoveis"></div>
		<div class="container">
			<div class="row">
				<div class="col-2">
					<div class="textbox-left">
						<h2>Conheça nosso Consórcio <p class="dark-blue">de IMÓVEIS</p></h2>
						<img src="<?php echo $dir. '/dist/img/bar-red.png' ?>" alt="bar-red">

						<p class="text-left">Você sonha em aumentar seu patrimônio, quitar seu financiamento imobiliário ou sair do aluguel? Então o Consórcio de Imóvel da Porto Seguro foi feito para você. Para aumentar seu patrimônio, sair do aluguel ou então construir ou reformar.</p>
						
						<span class="redutor blue-imovel">
							Parcelas a partir de R$ 349,00
						</span>

						<p class="text-left">Você ainda pode utilizar seu FGTS para ofertar um lance ou mesmo complementar seu crédito. Veja como é simples. <strong>Faça uma Simulação Rápida:</strong></p>

						<div class="select-parcela">	
							<select name="parcela" id="parcela-imovel">
								<?php 
								$imoveis = new WP_Query(
									[
										'post_type'			=> 'valores',
										'posts_per_page'	=> -1,
										'tax_query'			=> [
											[
												'taxonomy'		=> 'tipos-de-credito', 
												'field'			=> 'term_id',
												'terms'			=> 4
											]
										],
									]
								);
								if ( $imoveis->have_posts() ) {
									$psts = $imoveis->posts; 
									$i = 0;
									foreach ( array_reverse($psts) as $p ) {
										$i++;
										if ( $i == 0 ) {
											echo '<option class="opt" selected value="' . $p->ID . '">' . $p->post_title . '</option>';
										} else {
											echo '<option class="opt" value="'.$p->ID.'">'.$p->post_title.'</option>'; 
										}
										$id = $p->ID;
									}									
								}
								wp_reset_postdata();
								?>

							</select>
							<button class="btn-parcela imoveis" data-type="Imóveis" name="parcela">Ver valor da parcela</button>
						</div>
					</div>
				</div>	
				<div class="items-imoveis">
					<div class="container">
						<div class="row">
							<div class="slider">
								<div class="col-1">
									<img class="img-items" src="<?php echo $dir. '/dist/img/items/calendar.png'; ?>" alt="Créditos">
									<p class="item-desc-col">Até <strong>200 meses</strong> para você pagar</p>
								</div>
								<div class="col-1">
									<img class="img-items" src="<?php echo $dir. '/dist/img/items/moneyadd.png'; ?>" alt="Créditos">

									<p class="item-desc-col">Créditos a partir de <strong>55 mil</strong></p>
								</div>
								<div class="col-1">
									<img class="img-items" src="<?php echo $dir. '/dist/img/items/paint.png'; ?>" alt="Créditos">
									
									<p class="item-desc-col">Compra, reforma ou Construção</p>
								</div>
								<div class="col-1">
									<img class="img-items" src="<?php echo $dir. '/dist/img/items/house.png'; ?>" alt="Créditos">
									<p class="item-desc-col">Residencial, Comercial, Veraneio ou Terreno</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="financas">
		<div class="background-fin"></div>
		<div class="container">
			<div class="row">
				<div class="title-fin">
					<h3>Veja o Comparativo de <p><strong>Financiamento</strong> x <strong>Consórcio</strong></p></h3>	
					<img src="<?php echo $dir. '/dist/img/bar-red.png' ?>" alt="bar-red">
					<p class="infos-table">
						<span><img src="<?php echo $dir. '/dist/img/icons/icon-consorcio.jpg'; ?>" alt="Consórcio"> Consórcio | <img src="<?php echo $dir. '/dist/img/icons/icon-financiamento.jpg'; ?>" alt="Financiamento"> Financiamento</span>
					</p>

					<div class="table">
						<?php get_template_part( 'table' ); ?>
					</div>
				</div>
			</div>
		</section>
		<section id="automoveis">
			<div class="background-auto"></div>
			<div class="container">
				<div class="row">
					<div class="col-2">
						<div class="textbox-left">
							<h4>Conheça nosso Consórcio <p class="dark-blue">de AUTOMÓVEIS</p></h4>
							<img src="<?php echo $dir. '/dist/img/bar-red.png' ?>" alt="bar-red">

							<p class="text-left">A Porto Seguro oferece diversas opções para quem deseja comprar ou trocar de carro. Basta escolher o plano mais adequado aos seus sonhos.</p>
							<p class="text-left">Quer comprar um carro 0km ou trocar o seu por um novo? A solução mais econômica é o Porto Seguro Consórcio¹. Além disso, você pode contar com grupos de até 72 meses para pagar, com parcelas que cabem no seu bolso. <strong>Faça uma Simulação Rápida:</strong></p>

							<div class="select-parcela">	
								<select name="parcela" id="parcela-auto">
									<?php 
									$autos = new WP_Query(
										[
											'post_type'			=> 'valores',
											'posts_per_page'	=> -1,
											'orderby'			=> 'title', 
											'order'				=> 'DESC',
											'tax_query'			=> [
												[
													'taxonomy'		=> 'tipos-de-credito', 
													'field'			=> 'term_id',
													'terms'			=> 2
												]
											],
										]
									);
									// wrapInPre($autos->found_posts, true);
									if ( $autos->have_posts() ) {
										$psts = $autos->posts; 

										$arr50 = array_filter($psts,function($item) {
											if (strpos($item->post_title, '(50 meses)') && isset($item->post_title)){
												return $item;
											}
										});

										$arr75 = array_filter($psts,function($item) {
											if (strpos($item->post_title, '(72 meses)') && isset($item->post_title)){
												return $item;
											}
										});

										$result = array_merge($arr50,['jump' => true],  $arr75);
										$i = 0; 
										$val = '';
										$val2= '';
										//var_dump($result); die();
										foreach ( array_reverse($result) as $p ) {
											$i++; 

											if( $p->ID === 48){
												$val2 = $p;
											}

											//12
											if ( $i === 13 ) {
												echo '<option class="opt" value="'.$val2->ID.'">'.$val2->post_title.'</option>';
												echo '<option disabled></option>';
											}

											if ( $p->ID === 49) {
												$val = $p;
											}

											else {
												//12
												if ($i === 13){
													echo '<option class="opt" value="'.$val->ID.'">'.$val->post_title.'</option>';
													continue;
												}
												if (array_key_exists('jump', $p) ) {
													echo '<option class="opt" value="'.$p->ID.'">'.$p->post_title.'</option>';
													return true;
												} 
												else {
													if ( !strpos($p->post_title, '110.') ) {
														echo '<option class="opt" value="'.$p->ID.'">'.$p->post_title.'</option>';
													}
													if ( strpos($p->post_title, '99.8') ) {
														// echo '<option value="'.$p->ID.'">'.$pre-upload-ui>post_title.'</option>';
														echo '<option class="opt" value="58">Crédito de R$ 110.401,00 (72 meses)</option>';
													}	
												}
											}
										}
									}
									
									wp_reset_postdata();
									?>
								</select>
								<button class="btn-parcela autos" data-type="Automóveis">Ver valor da parcela</button>
							</div>
						</div>
					</div>
					<div class="items-auto">
						<div class="container">
							<div class="row">
								<div class="slider">
									<div class="col-1">
										<img class="img-items" src="<?php echo $dir. '/dist/img/items/calendar.png'; ?>" alt="Créditos">
										<p class="item-desc-col">Até <strong>72 meses </strong>para você pagar</p>
									</div>
									<div class="col-1">
										<img class="img-items" src="<?php echo $dir. '/dist/img/items/credits.png'; ?>" alt="Créditos">
										<p class="item-desc-col">Créditos a partir de <strong>25 mil</strong></p>
									</div>
									<div class="col-1">
										<img class="img-items" src="<?php echo $dir. '/dist/img/items/moneyback.png'; ?>" alt="Créditos">
										<p class="item-desc-col">Compra, ou troca de veículos</p>
									</div>
									<div class="col-1">
										<img class="img-items" src="<?php echo $dir. '/dist/img/items/car.png'; ?>" alt="Créditos">
										<p class="item-desc-col">Para carros de passeio ou utilitários</p>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</section>
		<section id="pesados">
			<div class="background-pesados"></div>
			<div class="container">
				<div class="row">
					<div class="col-2">
						<div class="textbox-right">
							<h4>Conheça nosso Consórcio de<p class="dark-blue">VEÍCULOS PESADOS</p></h4>
							<img src="<?php echo $dir. '/dist/img/bar-red.png' ?>" alt="bar-red">

							<p class="text-right">Expandir sua frota atual ou adquirir uma nova é uma opção para você dar o próximo passo no seu negócio. E o Porto Seguro Consórcio é o parceiro ideal para realizar seus planos.</p>
							<p class="text-right">Com ele, você conta com contemplações mensais por meio de lance ou sorteio e ainda pode utilizar até 30% do seu crédito para pagar o lance.</p>
							<p class="text-right">Solicite um contato e veja as vantagens do <strong>Consórcio de Caminhões, Ônibus e Máquinas Agrícolas.</strong></p>

							<div class="select-parcela">	
								<select name="parcela" id="parcela-pesado">
									<?php 
									$pesados = new WP_Query(
										[
											'post_type'			=> 'valores',
											'posts_per_page'	=> -1,
											'orderby'			=> 'title', 
											'order'				=> 'DESC',
											'tax_query'			=> [
												[
													'taxonomy'		=> 'tipos-de-credito', 
													'field'			=> 'term_id',
													'terms'			=> 3
												]
											],
										]
									);
									if ( $pesados->have_posts() ) {
										$psts = $pesados->posts; 

										$i = 0;
										foreach (array_reverse($psts) as $p ) {
											if ( $i == 0 ) {
												echo '<option class="opt" selected value="' . $p->ID . '">' . $p->post_title . '</option>';
											} else {
												echo '<option class="opt" value="'.$p->ID.'">'.$p->post_title.'</option>'; 
											}
											
											// echo '<option class="opt" value="'.$p->ID.'">'.$p->post_title.'</option>'; 

											$i++;
											$id = $p->ID;

											echo $i;
											echo ' '.$id;
										}
									}
									wp_reset_postdata();
									?>
								</select>
								<button class="btn-parcela pesados" data-type="Pesados">Ver valor da parcela</button>
							</div>
						</div>
					</div>
					<div class="items-pesados">
						<div class="slider">
							<div class="col-1">
								<img class="img-items" src="<?php echo $dir. '/dist/img/items/calendar.png'; ?>" alt="Créditos">
								<p class="item-desc-col">Até <strong>120 meses</strong> para você pagar</p>
							</div>
							<div class="col-1">
								<img class="img-items" src="<?php echo $dir. '/dist/img/items/moneyadd.png'; ?>" alt="Créditos">		
								<p class="item-desc-col">Créditos de <strong>150 mil</strong> até <strong>300 mil</strong></p>
							</div>
							<div class="col-1">
								<img class="img-items" src="<?php echo $dir. '/dist/img/items/moneyback.png'; ?>" alt="Créditos">
								<p class="item-desc-col">Contemplações mensais por meio de Sorteio, Lance Fixo ou Lance Livre</p>
							</div>
							<div class="col-1">
								<img class="img-items" src="<?php echo $dir. '/dist/img/items/wallet.png'; ?>" alt="Créditos">
								<p class="item-desc-col">Sem taxa de Adesão</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="sonhos">
			<div class="background-sonhos"></div>
			<div class="container">
				<div class="row">
					<div class="txt-sonhos">
						<h4 class="dark-title">Invista nos seus sonhos</h4>
						<p class="dark-desc">Fale agora com um de nossos consultores</p>	
						<img src="<?php echo $dir. '/dist/img/bar-red.png' ?>" alt="bar-red">
						<button class="btn-blue">Solicitar contato</button>	<div class="car">
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="porto-seguro">
			<div class="container">
				<div class="row">
					<div class="col-2">
						<div class="textbox-left">
							<h4>Saiba mais sobre o Consórcio<p class="dark-blue">PORTO SEGURO</p></h4>
							<img src="<?php echo $dir. '/dist/img/bar-red.png' ?>" alt="bar-red">

							<p class="text-left">Se você quer mais do que um imóvel e um automóvel, conheça o Porto Seguro Consórcio. Com ele, você realiza os seus projetos e não paga juros.
							</p>
							<p class="text-left">Utilize o seu FGTS¹ no caso da compra de imóveis e ainda participe de contemplações mensais por lance ou sorteio. O Porto Seguro Consórcio é para quem se planeja e quer fazer sempre um bom negócio.</p>

							<p class="title-text-left">Como funciona o consórcio?</p>

							<p class="text-left">O Consórcio é uma forma de se adquirir um bem, fazer um investimento ou de proporcionar a você a programação da compra do sonho de consumo, com menor gasto, se comparado à maioria dos financiamentos.</p>

							<p class="text-left">É similar a um financiamento, em que pessoas com o objetivo de adquirir um bem similar se juntam em um grupo e realizam pagamentos mensais para geração de fundo para compra do bem.</p>
						</div>
					</div>
					<div class="container">
						<div class="col-2">
							<button class="btn-video">Assista nosso vídeo institucional</button>
							<div class="video">
								
								<iframe src="https://www.youtube.com/embed/kLYStLxCc6Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

							</div>					
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="vendas">
			<div class="background-vendas"></div>
			<div class="container">
				<div class="col-2">
					<div class="textbox-left vendas">
						<h4>Fale conosco<p class="dark-blue">CENTRAL DE VENDAS</p></h4>
						<img src="<?php echo $dir. '/dist/img/bar-red.png' ?>" alt="bar-red">
						<p class="text-left txt-vendas">Veja como é simples. Entre em contato conosco através da central de vendas ou nos envie um e-mail para <a href="mailto:contato@consorciotransparente.com.br">contato@consorciotransparente.com.br</a>.
						</p>
					</div>
				</div>
				<div class="items-vendas">
					<div class="col-1">
						<img class="img-vendas" src="<?php echo $dir. '/dist/img/items/call.png'; ?>" alt="Créditos">
						<p class="item-desc-col">Ligue para</p>
						<p class="number"><a target="_blank" class="footer-telefone" href="tel:+551235123927">(12) 3512-3927</a></p>
					</div>
					<!-- <div class="col-1">
						<img class="img-vendas" src="<?php echo $dir. '/dist/img/items/whatsapp.png'; ?>" alt="Créditos">
						<p class="item-desc-col">Whatsapp</p>
						<p class="number"><a target="_blank" class="footer-whatsapp" href="wpp">(12) 99152-9797</a></p>
					</div> -->
				</div>
			</div>
		</section>
	</div>

</div>
</main>
<?php get_footer(); ?>