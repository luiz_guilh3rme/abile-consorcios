<?php 
## variables ##
$dir = get_template_directory_uri();

## theme supports ##
####################

if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );
    // add_image_size('example', 800, 450, true);
}

## BASE STYLESHEET AND JS FILES ## 
##################################

function registerScripts() {
	// base concatenated app.js (contains all non-cdn vendors, look at readme.md, gulpfile.js and src/js folders if you have any questions);
	wp_register_script( 'app', get_bloginfo('template_url') . '/dist/app.js', '', null, false );
	wp_register_script('sweet-alert', 'https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js', null, false);
	
	wp_enqueue_script('app');
	wp_enqueue_script('sweet-alert');
	/*Localize script dir at g.mailsend.js*/

$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
//after wp_enqueue_script
wp_localize_script( 'app', 'object_name', $translation_array );
}

add_action( 'wp_enqueue_scripts', 'registerScripts' );

function registerStyles() {
    // base stylesheet (contains all non-cdn vendors, look at readme.md, gulpfile.js, src/main.css and src/stylus)
	wp_register_style( 'basic-stylesheet', get_bloginfo('template_url') . '/dist/main.css', '',  null, 'all' );
	wp_register_style('slick', get_bloginfo('template_url'). '/dist/slick.min.css', null, 'all');

	wp_enqueue_style( 'basic-stylesheet');
	wp_enqueue_style( 'slick' );

}

add_action( 'wp_enqueue_scripts', 'registerStyles' );

## GENERIC FUNCTIONS ## 
#######################

// generic excerpt function
// $instance being WHAT you want to excerpt (ex: get_the_content())

function excerpt($limit, $instance) {
	$excerpt = explode(' ', $instance, $limit);
	if ( count( $excerpt ) >= $limit ) {
		array_pop( $excerpt );
		$excerpt = implode(" ", $excerpt ).' ...';
	} 
	else {
		$excerpt = implode( " ", $excerpt );
	}	
	$excerpt = preg_replace( '`[[^]]*]`', '', $excerpt );
	return $excerpt;
}


// send object to app.js via wp_localize_script
// look at src/js/x.helpers.js to find the JS handling
function hoistObject() {

	// URLS
	$url = get_bloginfo('template_url');
	$adminURL = admin_url('admin-ajax.php');
	$homeURL = home_url('/');

	// create array
	$hoist = json_encode(array(
		'url' => $url,
		'adminURL' => $adminURL,
		'homeURL' => $homeURL,
	));

	if (!empty($hoist) ) {
		wp_localize_script('app', 'Hoist', $hoist);
	}

}

add_action('wp_enqueue_scripts', 'hoistObject');


// Wrap object in <pre> tags for easily viewing what you are receiving from a query 

function wrapInPre($obj, $death = false, $color = 'black') {

	if (empty($obj)) {
		return false;
	}

	echo '<pre style="font-size: 22px; color: '.$color.';">';
	print_r($obj); 
	echo '</pre>';

	if ($death) {
		die;
	}
}


## POST TYPES ## 
################

function create_posttypes() {
	$valueArgs = array(
		'name'               => _x( 'valores', 'post type general name'),
		'singular_name'      => _x( 'valor', 'post type singular name'),
		'menu_name'          => _x( 'valores', 'admin menu'),
		'name_admin_bar'     => _x( 'Adicionar valor', 'add new on admin bar'),
		'add_new'            => _x( 'Adicionar novo', ''),
		'add_new_item'       => __( 'Adicionar novo valor'),
		'new_item'           => __( 'Novo valor'),
		'edit_item'          => __( 'Editar valor'),
		'view_item'          => __( 'Ver valores'),
		'all_items'          => __( 'Todos valores'),
		'search_items'       => __( 'Buscar valores'),
		'not_found'          => __( 'Nenhum item encontrado'),
		'not_found_in_trash' => __( 'Nenhum item encontrado na lixeira.')
	);

	register_post_type( 'valores',
		array(
			'labels' => $valueArgs,
			'public' => true,
			'supports' => array('title', 'editor'),
		)
	);
}

## TAXONOMIES ## 
################

function registerTaxonomies() {
    // Tipo de Crédito
	$types = array(
		'name' => _x('Tipos de Crédito', 'taxonomy general name'),
		'singular_name' => _x('Tipo de Crédito', 'taxonomy singular name'),
		'search_items' => __('Procurar Tipo de Crédito'),
		'all_items' => __('Todos os Tipo de Créditos'),
		'edit_item' => __('Editar Tipo de Crédito'),
		'update_item' => __('Atualizar Tipo de Crédito'),
		'add_new_item' => __('Adicionar novo Tipo de Crédito'),
		'new_item_name' => __('Nome da novo Tipo de Crédito'),
		'menu_name' => __('Tipo de Créditos'),
	);

	$typeArgs = array(
		'labels' 			=> $types,
		'hierarchical' 		=> true,
		'public' 			=> true,
		'show_ui' 			=> true,
		'show_in_menu' 		=> true,
		'show_admin_column' => true,
		'query_var' 		=> true,
	);

	register_taxonomy('tipos-de-credito', 'valores', $typeArgs);
}

add_action('init', 'registerTaxonomies');

add_action( 'init', 'create_posttypes' );

## GENERAL CHANGES TO WP "CORE" ## 
##################################


// remove welcome panel
remove_action('welcome_panel', 'wp_welcome_panel');

// de-register useless wp scripts

function deregister_scripts() {
	wp_deregister_script( 'wp-embed' );

	if ( !is_admin() ) {

        // i do this because i generally concatenate jquery into my app.js
		wp_dequeue_script( 'jquery' );
		wp_deregister_script( 'jquery' );
	}
}

add_action( 'wp_enqueue_scripts', 'deregister_scripts' );

// remove wrapping span elements on cf7 fields
add_filter('wpcf7_form_elements', function($content) {
	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

	return $content;
});

// remove autop from CF7 (5.0 +) 

add_filter( 'wpcf7_autop_or_not', '__return_false' );


// remove margin top from admin bar

function remove_admin_login_header()
{
	remove_action('wp_head', '_admin_bar_bump_cb');
}

add_action('init', 'remove_admin_login_header');

/**
 * Disable the emoji's
*/

function disable_emojis() {

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );

}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param    array  $plugins  
 * @return   array             Difference betwen the two arrays
 */

function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

// disable the admin bar
add_filter('show_admin_bar', '__return_false');
show_admin_bar(false);

function hideAdminBar() { ?>
	<style type="text/css">.show-admin-bar { display: none; }</style>
<?php }
add_action('admin_print_scripts-profile.php', 'hideAdminBar');


// remove cf7 and gutenberg styles
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

function wps_deregister_styles() {
	wp_deregister_style( 'contact-form-7' );
	wp_dequeue_style( 'wp-block-library' );
}

// theme vars
set_theme_mod('whatsapp', '(12) 99152-9797'); 
set_theme_mod('whatsappLink', 'https://api.whatsapp.com/send?phone=5512991529797');
set_theme_mod('telefone', '(12) 3512-3927');
set_theme_mod('telefoneLink', 'tel:+551235123927');

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5d0d280e087a8',
	'title' => 'Parcela PF',
	'fields' => array(
		array(
			'key' => 'field_5d0d28142e64e',
			'label' => 'Parcela PF',
			'name' => 'parcela_pf',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5d10c4dfe7116',
			'label' => 'Parcela PJ',
			'name' => 'parcela_pj',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'valores',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;