
<table class="tg" style="table-layout: fixed; width: 786px">
  <colgroup>
    <col style="width: 200px">
    <col style="width: 140px">
    <col style="width: 180px">
    <col style="width: 180px">
    <col style="width: 360px">
  </colgroup>
  <tr class="table-top">
    <th></th>
    <th class="th-blue">Valor do Imóvel</th>
    <th class="th-darkblue">Valor do Imóvel</th>
    <th class="th-blue">Valor Total</th>
    <th class="th-darkblue">Total Economizado Utilizando o Consórcio</th>
  </tr>
  <tr class="table-body">
    <td class="first-column">Financiamento</td>
    <td class="th-darkblue" rowspan="2">R$ 250.000,00</td>
    <td class="val-red">204 x R$ 2.962,78</td>
    <td class="val-red">R$ 604.407,12</td>
    <td class="tab-green" rowspan="2">R$ 295.099,12</td>
  </tr>
  <tr class="table-body">
    <td class="first-column">Consórcio</td>
    <td class="val-gray">200 x R$ 1.546,54</td>
    <td class="val-gray">R$ 309.308,00</td>
  </tr>
  <tr class="table-body">
    <td class="first-column">Financiamento</td>
    <td class="th-darkblue" rowspan="2">R$ 500.000,00</td>
    <td class="val-red">204 x R$ 5.925,56</td>
    <td class="val-red">R$ 1.208.814,24</td>
    <td class="tab-green" rowspan="2">R$ 590.198,24</td>
  </tr>
  <tr class="table-body">
    <td class="first-column">Consórcio</td>
    <td class="val-gray">200 x R$ 3.093,08</td>
    <td class="val-gray">R$ 618.616,00</td>
  </tr>
  <tr class="table-body">
    <td class="first-column">Financiamento</td>
    <td class="th-darkblue" rowspan="2">R$ 1.000.000,00</td>
    <td class="val-red">204 x R$ 11.851,12</td>
    <td class="val-red">R$ 2.417.628,48</td>
    <td class="tab-green" rowspan="2">R$ 1.180.396,48</td>
  </tr>
  <tr class="table-body">
    <td class="first-column">Consórcio</td>
    <td class="val-gray">200 x R$ 6.186,16</td>
    <td class="val-gray">R$ 1.237.232,00</td>
  </tr>

</table>
<div class="slider">
  <table class="table-mobile">
    <tr>
      <th class="title-imovel">Valor do Imóvel</th>
    </tr>
    <tr>
      <td class="th-darkblue">R$ 250.000,00 </td>
    </tr>
    <tr>
      <td class="th-blue">Valor da Parcela</td>
    </tr>
    <tr>
      <td class="val-red">204 x R$ 2.962,78</td>
    </tr>
    <tr>
      <td class="val-gray">200 x R$ 1.546,54</td>
    </tr>
    <tr>
      <td class="th-blue">Valor Total</td>
    </tr>
    <tr>
      <td class="val-red">R$ 604.407,12</td>
    </tr>
    <tr>
      <td class="val-gray">R$ 309.308,00
      </td>
    </tr>
    <tr>
      <td class="th-darkblue">Total Economizado Utilizando o Consórcio<br></td>
    </tr>
    <tr>
      <td class="tab-green">R$ 295.099,12</td>
    </tr>
  </table>

  <table class="table-mobile">
    <tr>
      <th class="title-imovel">Valor do Imóvel</th>
    </tr>
    <tr>
      <td class="th-darkblue">R$ 500.000,00 </td>
    </tr>
    <tr>
      <td class="th-blue">Valor da Parcela</td>
    </tr>
    <tr>
      <td class="val-red">204 x R$ 5.925,56</td>
    </tr>
    <tr>
      <td class="val-gray">200 x R$ 3.093,08</td>
    </tr>
    <tr>
      <td class="th-blue">Valor Total</td>
    </tr>
    <tr>
      <td class="val-red">R$ 1.208.814,24</td>
    </tr>
    <tr>
      <td class="val-gray">R$ 618.616,00</td>
    </tr>
    <tr>
      <td class="th-darkblue">Total Economizado Utilizando o Consórcio<br></td>
    </tr>
    <tr>
      <td class="tab-green">R$ 590.198,24</td>
    </tr>
  </table>

  <table class="table-mobile">
    <tr>
      <th class="title-imovel">Valor do Imóvel</th>
    </tr>
    <tr>
      <td class="th-darkblue">R$ 1.000.000,00</td>
    </tr>
    <tr>
      <td class="th-blue">Valor da Parcela</td>
    </tr>
    <tr>
      <td class="val-red">204 x R$ 11.851,12</td>
    </tr>
    <tr>
      <td class="val-gray">200 x R$ 6.186,16</td>
    </tr>
    <tr>
      <td class="th-blue">Valor Total</td>
    </tr>
    <tr>
      <td class="val-red">R$ 2.417.628,48</td>
    </tr>
    <tr>
      <td class="val-gray">R$ 1.237.232,00</td>
    </tr>
    <tr>
      <td class="th-darkblue">Total Economizado Utilizando o Consórcio<br></td>
    </tr>
    <tr>
      <td class="tab-green">R$ 1.180.396,48</td>
    </tr>
  </table>

</div>