	<div class="overlay-parcela">
		<button class="close-modal close-cta-forms close-parcela" aria-label="Fechar Modal">
			&times;
		</button>
		<div class="form-wrapper-parcela">
			<div class="leave-message">
				<legend class="leave-title">
					Preencha os campos abaixo para fazer uma simulação.
				</legend>
				<div class="pop-box">
					<img class="img-pig" src="<?php echo get_template_directory_uri(). '/dist/img/pig.png'; ?>" alt="Cofrinho em formato de porco">
					<form class="box-message form-parcela" data-modal="box-parcela">
						<input type="hidden" name="formulario" class="form-issue" value="">
						<input type="hidden" name="credito" value="">
						<input type="text" name="nome" class="boxes" placeholder="Informe seu nome" required>
						<input type="phone" name="telefone" class="boxes" required minlength=14 placeholder="Informe seu telefone">
						<input type="email" name="email" class="boxes" placeholder="Informe seu e-mail" required>
						<button type="submit" name="parcela" class="btn-msgsub">
							<p>Ver valor da parcela</p>
						</button>
					</form>
				</div>
				<div class="fields cleared">
					<p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
				</div>
			</div>
		</div>
	</div>
