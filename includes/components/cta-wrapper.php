<div class="call-cta-wrapper">
	<div class="cta-tooltip">
		<p class="tooltip-text">
			<p class="main-txt">Olá!</p>
			<p class="desc">Gostaria de receber uma ligação?</p>
		</p>
		<button class="confirm">SIM</button>
		<button class="close-cta" aria-label="Fechar CTA de whatsapp">
			&times;
		</button>
	</div>
	<a target="_BLANK" href="wpp" class="btn-whatsapp">
		<button class="whats-icon">
			<img src="<?php echo get_template_directory_uri().'/dist/img/icons/zap.png'; ?>" alt="Whatsapp">
		</button>
	</a>
	<button class="phone-icon">
		<img src="<?php echo get_template_directory_uri().'/dist/img/icons/phone.png'; ?>" alt="Telefone">
	</button>
</div>
<div class="cta-overlay">
	<button class="close-modal close-cta-forms" aria-label="Fechar Modal">
		&times;
	</button>
	<div class="form-wrapper-all">
		<div class="form-picker">
			<button class="form-pickers" data-instance="00">
				<img src="<?php echo get_template_directory_uri().'/dist/img/icons/icon-phone.png'; ?>" alt="Telefone">
				<p>ME LIGUE AGORA</p>
			</button>
			<button class="form-pickers" data-instance="01">
				<img src="<?php echo get_template_directory_uri().'/dist/img/icons/icon-clock.png'; ?>" alt="Relógio">
				<p>ME LIGUE DEPOIS</p>
			</button>
			<button class="form-pickers active" data-instance="02">
				<img src="<?php echo get_template_directory_uri().'/dist/img/icons/icon-message.png'; ?>" alt="Mensagem">
				<p>DEIXE UMA MENSAGEM</p>
			</button>
		</div>
		<div class="instance" data-instance="00">
			<div class="leave-message">
				<legend class="leave-title">
					<span class="variant">
						NÓS TE LIGAMOS!
					</span>
					Informe seu telefone que entraremos em
					contato o mais rápido possível.
				</legend>
				<div class="pop-box">
					<form class="box-message" data-modal="box-nos-te-ligamos">
						<input type="hidden" name="formulario" value="Nós te Ligamos">
						<input type="text" name="nome" class="boxes" placeholder="Informe seu nome" required>
						<input type="phone" name="telefone" required minlength=14 class="boxes" placeholder="Informe seu telefone" required>
						<button type="submit" name="btn-agora" class="btn-msgsub">
							<p>Me ligue agora</p>
						</button>
					</form>
				</div>
				<div class="fields cleared">
					<p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
				</div>
			</div>
		</div>
		<div class="instance" data-instance="01">
			<div class="leave-message schedule-time">
				<legend class="leave-title">
					Gostaria de agendar e receber uma
					chamada em outro horário?
				</legend>
				<div class="pop-box">
					<form action="box-message" class="box-message" data-modal="box-agendar-horario">
						<input type="hidden" name="formulario" value="Agendar Horário">
						<input type="text" name="data" class="data box-date" placeholder="Data" required>
						<input type="text" name="hora" class="box-date" placeholder="11H30" required>
						<input type="text" name="nome" class="boxes" placeholder="Informe seu nome" required>
						<input type="phone" name="telefone" required minlength=14 class="boxes" placeholder="Informe seu telefone" required>
						<button type="submit" name="btn-depois" class="btn-msgsub">
							<p>Me ligue depois</p>
						</button>
					</form>
				</div>

				<div class="fields cleared">

					<p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
				</div>
			</div>
		</div>
		<div class="instance active" data-instance="02">
			<div class="leave-message">
				<legend class="leave-title">
					Deixe sua mensagem! Entraremos em
					contato o mais rápido possível.
				</legend>
				<div class="pop-box">
					<form action="box-message" class="box-message" data-modal="box-deixar-mensagem">
						<input type="hidden" name="formulario" value="Deixe sua Mensagem">
						<textarea name="mensagem" class="box-textarea" cols="30" rows="5" placeholder="Deixe sua mensagem" required></textarea>
						<input type="text" name="nome" class="boxes" placeholder="Informe seu nome" required>
						<input type="phone" name="telefone" required minlength=14 class="boxes" placeholder="Informe seu telefone" required>
						<button type="submit" name="btn-msg" class="btn-msgsub">
							<p>Enviar mensagem</p>
						</button>
					</form>
				</div>
				<div class="fields cleared">
					<p class="callers">Você já é a <span class="number">3</span> pessoa a deixar uma mensagem.</p>
				</div>
			</div>
		</div>
	</div>
</div>