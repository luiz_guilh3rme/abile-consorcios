<?php
require '../vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use PHPMailer\PHPMailer\PHPMailer;

		//$terms = wp_get_post_terms($id, 'tipos-de-credito');
		// $credit = get_post($id)->post_title; 
		// var_dump(get_post($id));

if ( isset( $_POST ) ) {
	try {

		$fields = $_POST;
		$subject = isset( $_POST['formulario'] ) ? $_POST['formulario'] . ' | Formulário do Site' : 'Formulário do site';
		
		$mail = new PHPMailer(true);
		$mail->CharSet = 'UTF-8'; 
		$mail->Subject = $subject;
		$mail->isHTML(true);
		$mail->setFrom('disparo@consorciodigitalonline.com.br', 'Disparo'); 
		$mail->addAddress('contato@consorciodigitalonline.com.br', 'Contato');
		$mail->addAddress('karine@abilecorretoradeseguros.com.br', 'Karine');
		$mail->addAddress('thiago@abilecorretoradeseguros.com.br', 'Thiago');
		
		$mail_body = '<html>';
		$mail_body .= '<table style="font-size: 16px; font-family: Helvetica, sans-serif; border: 1px solid; border-collapse: collapse;">'; 
		
		foreach ( $fields as $field => $f ) {
			$mail_body .= 
			'<tr>
			<td style="padding: 10px; border: 1px solid; font-weight: bold">'.$field.'</td>
			<td style="padding: 10px; border: 1px solid;">'.$f.'</td>
			</tr>';
		} 
		
		$mail_body .= '</table>';
		$mail_body .= '</html>';

		$mail->Body = $mail_body;

		$mail->send();

	} catch (Exception $e) {
		echo '<pre>'; 
		print_r($e);
		echo '</pre>';
		die;
	}
}
		