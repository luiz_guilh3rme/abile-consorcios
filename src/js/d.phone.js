function openPhoneCta() {
	// duhh
	$('.confirm, .mobile-cta-btn, .btn-blue').on('click', function () {

		$('.cta-overlay').fadeIn(400, function () {
			$('.form-wrapper-all').fadeIn();
		});
		$('html, body').css('overflow', 'hidden');
		$('html, body').animate({
			scrollTop: 0
		}, 800);
		return false;

		var now = new Date(),
		days = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		day = days[now.getDay()];
		hour = now.getHours(),
		picker = $('.form-pickers[data-instance=01]');


		if (day === 'Domingo' || day === 'Sábado' || hour > 18 || hour < 8) {
			picker[0].disabled = true;
			picker[0].title = "Disponível apenas em horário comercial";
		}
	});
}

function closeModalCta() {
	$('.close-modal').on('click', function () {
		$('.form-wrapper-all').fadeOut(400, function () {
			$('.cta-overlay').fadeOut();
		});
		$('html, body').css('overflow-y', 'initial');
	});
}

function closeCTA() {
	$('.cta-tooltip').addClass('clicked');
}

function reopenCTA() {
	$('.cta-tooltip').removeClass('clicked');
}

function formPickers() {
	$('.form-pickers').removeClass('active');
	$(this).addClass('active');
	var instance = $(this).data('instance');

	$('.instance:not([data-instance=' + instance + '])').fadeOut(100, function () {
		setTimeout(function () {
			$('.instance[data-instance=' + instance + ']').fadeIn()
		}, 200)
	});
}

function openModalParcela(){
	$('.btn-parcela').on('click', function () {

		var selectedOption = $(this).siblings('select[name="parcela"]').val();
		var selectedText = $('select#parcela-imovel option:selected').html();
		var selectedText2 = $('select#parcela-auto option:selected').html();
		var selectedText3 = $('select#parcela-pesado option:selected').html();

		window.selectedOption = selectedOption;

		if( $( $(this) ).hasClass('imoveis') ){
			$('.form-issue').val("Formulário - Consórcio de Imóveis ");
			$('input[name="credito"]').val(selectedText);
					// console.log(selectedText);
		}
		else if( $( $(this) ).hasClass('autos') ){
			$('.form-issue').val("Formulário - Consórcio de Automóveis ");
			$('input[name="credito"]').val(selectedText2);
				// console.log(selectedText2);
		}
		else if( $( $(this) ).hasClass('pesados') ){
			$('.form-issue').val("Formulário - Consórcio de Veículos Pesados ");
			$('input[name="credito"]').val(selectedText3);
			// console.log(selectedText3);
		}

		$('.overlay-parcela').fadeIn(400, function(){
			$('.form-wrapper-parcela').fadeIn();
		});

		$('html, body').css('overflow', 'hidden');
		$('html, body').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
}

function closeModalParcela(){
	$('.close-parcela').on('click', function () {

		$('.form-wrapper-parcela').fadeOut(400, function () {
			$('.overlay-parcela').fadeOut();
		});
		
		$('.form-issue').val('');

		$('html, body').css('overflow-y', 'initial');
	});
}
function loadBox(){
	$('.form-pickers').on('click', formPickers);
	$('.close-cta').on('click', closeCTA); 
	$('.phone-icon').on('click', reopenCTA);
}