
  $.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function () {
    var templateUrl = object_name.templateUrl;
    var origin = window.location.origin;


    $('body').on('submit', 'form', function (event) {
        event.preventDefault();

            // set generic variables
            var fields = $(this).serializeObject(),
            instance = $(this).data('modal'),
            mailInstance,
            redirect; 

            // sort form instance
            switch (instance) {

                // main-form
                case 'form-proposta':
                mailInstance = templateUrl + '/includes/forms-submit.php';
                // redirect = origin + 'obrigado-proposta/';
                redirect = origin + '/obrigado/';
                break;

                // box-parcela
                case 'box-parcela':
                mailInstance    = templateUrl + '/includes/forms-submit.php';
                // redirect        = origin + 'obrigado-parcela/';
                redirect = origin + 'obrigado/';
                var selectedOption  = window.selectedOption;
                var queryString     = encodeURIComponent(selectedOption);
                break;

                // box-deixar-mensagem
                case 'box-deixar-mensagem':
                mailInstance = templateUrl + '/includes/forms-submit.php';
                // redirect = origin + 'sucesso-mensagem/';
                redirect = origin + '/obrigado/';
                break;    

                // box-ligue-depois
                case 'box-agendar-horario':
                mailInstance = templateUrl + '/includes/forms-submit.php';
                // redirect = origin + 'sucesso-ligue-depois/';
                redirect = origin + '/obrigado/';
                break;    

                // box-ligar-agora
                case 'box-nos-te-ligamos':
                mailInstance = templateUrl + '/includes/forms-submit.php';
                // redirect = origin + 'sucesso-ligue-agora/';
                redirect = origin + '/obrigado/';
                break;    

                default:
                return false;
            }

            if ( selectedOption !== undefined ) {

                redirect += '?id=' + queryString + '';

            }

            // ajax to email submit
            $.ajax({
                url: mailInstance,
                type: 'POST',
                data: fields,
            })
            .done(function (r) {
                setTimeout(function () {
                    window.location =  redirect;
                }, 800);
            })
            .fail(function () {
                swal({
                    title: 'Algo deu errado...',
                    text: 'Tente novamente mais tarde ou entre em contato através de nossos telefones.',
                    icon: 'error',
                })
            })
            .always(function () {
                // console.log(fields); 
            });
        });
});




