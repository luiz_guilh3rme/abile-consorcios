// unique scripts go here.
function loadMask() {
	var SPMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		spOptions = {
			onKeyPress: function (val, e, field, options) {
				field.mask(SPMaskBehavior.apply({}, arguments), options);
			}
		};


	$('input[name="celular"], input[name="telefone"]').mask(SPMaskBehavior, spOptions);
	$('.box-date').mask('00h00');
	$('.data').mask('00/00/0000');
}

function sliderMobile(){

	$('.slider').slick({
		arrows:false,
		slidesToScroll: 1,
		mobileFirst:true,
		fade:true,
		dots:true,
		responsive:[{
			breakpoint: 1024,
			settings: "unslick"
		}
		]
	});
}

function loadForm(){
	$('.btn-form').click(function(){
		$('.box-form').fadeToggle('fast','linear');
	});
}

function showProposta(){
	$('.btn-mb').click(function(){
		$('.btn-mb').css('display', 'none');
		$('.form-box').fadeIn('slow');
		$('.box-proposta').css('height','550px');
		$('#imoveis').css('top','90px');
		$('#imoveis').css('height', '984px');
	});
}

function loadVideo(){
	$('.btn-video').click(function(){
		$('.video').fadeIn('slow');
		$('.vendas').css('margin-top', '440px');
		$('.background-vendas').css('height', '1200px');
		$('.background-vendas').css('background-position-y', '90%');
	});
}
